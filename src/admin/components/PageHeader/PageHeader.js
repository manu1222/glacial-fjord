import React from "react";

class PageHeader extends React.Component {
  render() {
    return (
      <div>
        <h4>{this.props.title}</h4>
        <p>{this.props.subtitle}</p>
      </div>
    );
  }
}

export default PageHeader;
