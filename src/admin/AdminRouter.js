import React from "react";

import AdminLayout from "routers/AdminLayout";
import DashboardPage from "./containers/DashboardPage/DashboardPage";
import SettingsPage from "./containers/SettingsPage/SettingsPage";
import "./styles/styles.scss";

const AdminRouter = () => (
  <div>
    <AdminLayout path="/admin" component={DashboardPage} exact={true} />
    <AdminLayout path="/admin/settings" component={SettingsPage} />
  </div>
);

export default AdminRouter;
