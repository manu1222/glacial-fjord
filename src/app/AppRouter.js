import React from "react";

import AppLayout from "routers/AppLayout";
import PublicRoute from "routers/PublicRoute";
import HomePage from "./containers/HomePage/HomePage";
import SettingsPage from "./containers/SettingsPage/SettingsPage";
import LoginPage from "auth/LoginPage";

const AppRouter = () => (
  <div>
    <AppLayout path="/" component={HomePage} exact={true} />
    <AppLayout path="/settings" component={SettingsPage} />

    <PublicRoute path="/login" component={LoginPage} />
  </div>
);

export default AppRouter;
