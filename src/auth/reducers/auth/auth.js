import { LOGIN, LOGOUT } from "auth/actions/actionTypes.js";

const initialState = {
  uid: localStorage.getItem("uid")
};

export default (state = initialState, action) => {
  switch (action.type) {
    case LOGIN:
      return {
        uid: action.uid
      };
    case LOGOUT:
      return {};
    default:
      return state;
  }
};
