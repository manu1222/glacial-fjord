import React from "react";
import { Link, NavLink } from "react-router-dom";

class Header extends React.Component {
  render() {
    return (
      <header className="header">
        <div className="content-container">
          <div className="header__content">
            <Link className="header__title" to="/">
              <h1>App</h1>
            </Link>
            <Link className="header__title" to="/admin">
              <h1>Admin</h1>
            </Link>

            <NavLink
              to="/admin/settings"
              activeClassName="active"
              className="ap-menu-link"
            >
              <div className="header__title">
                <i className="menu-item-icon fa fa-user tx-22" />
                <span className="menu-item-label">Settings</span>
              </div>
            </NavLink>
            <button className="button button--link">Logout</button>
          </div>
        </div>
      </header>
    );
  }
}

export default Header;
