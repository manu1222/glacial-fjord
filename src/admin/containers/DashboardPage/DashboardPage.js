import React, { Component } from "react";
import PageHeader from "admin/components/PageHeader/PageHeader";

class DashboardPage extends Component {
  render() {
    return (
      <div className="App">
        <PageHeader
          title="Dashboard"
          subtitle="Subtitle"
        />
      </div>
    );
  }
}

export default DashboardPage;
