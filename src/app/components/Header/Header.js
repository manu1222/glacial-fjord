import React from "react";
import { Link } from "react-router-dom";
import logo from "logo.svg";

const Header = () => {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <h1 className="App-title">Welcome to App</h1>
      </header>
      <p className="App-intro">
        <Link to="/" exact={true}>
          Dashboard
        </Link>
        &nbsp;
        <Link to="/settings">Settings</Link>
        &nbsp;
        <Link to="/login">Login</Link>
        <br />
        <Link to="/" exact={true}>
          Home React Link
        </Link>
        &nbsp;
        <Link to="/admin">Admin React Link</Link>
      </p>
    </div>
  );
};

export default Header;
