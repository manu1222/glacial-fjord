import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import "./index.css";
import RootRouter from "./routers/RootRouter";
import registerServiceWorker from "./registerServiceWorker";

import configureStore from "./store/configureStore";

const store = configureStore();

const jsx = (
  <Provider store={store}>
    <RootRouter />
  </Provider>
);

ReactDOM.render(jsx, document.getElementById("app"));
registerServiceWorker();
