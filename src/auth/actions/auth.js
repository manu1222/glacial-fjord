import { LOGIN, LOGOUT } from "auth/actions/actionTypes.js";

export const login = uid => ({
  type: LOGIN,
  uid
});

export const startLogin = uid => {
  return dispatch => {
    dispatch(login(uid));
    localStorage.setItem("uid", uid);
  };
};

export const logout = () => ({
  type: LOGOUT
});
