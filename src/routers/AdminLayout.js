import React from "react";
import { Route, Redirect } from "react-router-dom";
import { Helmet } from "react-helmet";
import { connect } from "react-redux";
import Header from "admin/components/Header/Header";

export const AdminLayout = ({ isAuthenticated, component: Component, ...rest }) => {
  return (
    <Route
      {...rest}
      component={props =>
        isAuthenticated ? (
          <section>
            <Helmet
              titleTemplate="%s - React.js Admin panel"
              defaultTitle="React.js Admin panel"
            >
              <meta name="description" content="React.js Admin panel" />
            </Helmet>
            <Header />
            <div className="main-panel">
              <Component {...props} />
            </div>
          </section>
        ) : (
          <Redirect to="/" />
        )
      }
    />
  );
};

const mapStateToProps = state => ({
  isAuthenticated: !!state.auth.uid
});

export default connect(mapStateToProps)(AdminLayout);
