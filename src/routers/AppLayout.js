import React from "react";
import { Route } from "react-router-dom";
import Header from "app/components/Header/Header";

const AppLayout = ({ component: Component, ...rest }) => {
  return (
    <Route
      {...rest}
      component={props => (
        <div>
          <Header />
          <Component {...props} />
        </div>
      )}
    />
  );
};

export default AppLayout;
