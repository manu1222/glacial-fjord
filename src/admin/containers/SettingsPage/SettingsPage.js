import React, { Component } from "react";
import PageHeader from "admin/components/PageHeader/PageHeader";

class SettingsPage extends Component {
  render() {
    return (
      <div className="App">
        <PageHeader
          title="Settings"
          subtitle="Subtitle"
        />
      </div>
    );
  }
}

export default SettingsPage;
