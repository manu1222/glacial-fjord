import React from "react";
import { Router } from "react-router-dom";
import createHistory from "history/createBrowserHistory";
import AppRouter from "app/AppRouter";
import AdminRouter from "admin/AdminRouter";

export const history = createHistory();

const RootRouter = () => (
  <Router history={history}>
    <div>
      <AppRouter />
      <AdminRouter />
    </div>
  </Router>
);
export default RootRouter;
