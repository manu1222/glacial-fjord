import React from "react";
import { connect } from "react-redux";
import { startLogin } from "auth/actions/auth";

export class LoginPage extends React.Component {
  loginHandler = () => {
    this.props.startLogin(1);
    this.props.history.push("/admin");
  };
  render() {
    return (
      <div className="box-layout">
        <div className="box-layout__box">
          <h1 className="box-layout__title">Awesome Project</h1>
          <p>It's time to get your expenses under control.</p>
          <button className="button" onClick={this.loginHandler}>
            Login
          </button>
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    startLogin: uid => dispatch(startLogin(uid))
  };
};

export default connect(null, mapDispatchToProps)(LoginPage);
